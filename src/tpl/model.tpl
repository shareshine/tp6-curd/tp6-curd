<?php

namespace app<namespace>model;

use think\model;

class <model> extends Model
{
    protected $name = '<table>';
    protected $pk = '<pk>';
    use SoftDelete;
    protected $deleteTime = 'delete_time';

}

