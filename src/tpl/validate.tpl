<?php

namespace app<namespace>validate;

use aipeng\curd\extend\ExtendValidate;

class <model>Validate extends ExtendValidate
{
    protected $rule = <rule>;

    protected $attributes = <attributes>;
}