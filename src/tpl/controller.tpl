<?php

namespace app<namespace>controller;

use app<namespace>model\<model> as <model>Model;
use app<namespace>validate\<model>Validate;
use think\exception\ValidateException;
use aipeng\curd\traits\CurdTrait;

class <controller> extends Base
{

    use CurdTrait;
    public $model = null;

    public function __construct()
    {
        $this->model = new <model>Model();
        $this->validateClass = <model>Validate::class;
    }

    
}
