<?php

\think\Console::starting(function (\think\Console $console) {
    $console->addCommands([
        'curd' => '\\aipeng\\curd\\command\\Curd'
    ]);
});



if (!function_exists('error')) {
    function error(string $msg = '操作失败', $err = [], int $code = 400)
    {
        $res['code'] = $code;
        $res['msg']  = $msg;
        $res['err']  = $err;

        // print_r(json_encode($res, JSON_UNESCAPED_UNICODE));
        json($res)->send();
        exit();
    }
}


/**
 * 成功返回
 *
 * @param array   $data 成功数据
 * @param string  $msg  成功提示
 * @param integer $code 成功码
 * 
 * @return json
 */
if (!function_exists('success')) {
    function success($data = [], string $msg = '操作成功', int $code = 200)
    {
        $res['code'] = $code;
        $res['msg']  = $msg;
        $res['data'] = $data;
        json($res)->send();
        exit();
    }
}
