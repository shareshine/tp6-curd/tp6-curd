<?php

namespace aipeng\curd\strategy;

use aipeng\curd\template\IAutoMake;

class AutoMakeStrategy
{
    protected $strategy;

    public function Context(IAutoMake $obj)
    {
        $this->strategy = $obj;
    }

    public function executeStrategy($flag, $path, $other)
    {
        $this->strategy->check($flag, $path);
        $this->strategy->make($flag, $path, $other);
    }
}