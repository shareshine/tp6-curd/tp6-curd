<?php

namespace aipeng\curd\template;

interface IAutoMake
{
    public function check($flag, $path);

    public function make($flag, $path, $other);
}