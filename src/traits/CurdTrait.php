<?php

declare(strict_types=0);

namespace aipeng\curd\traits;

use think\exception\ValidateException;
use think\facade\Db;

trait CurdTrait
{
    /**
     * 构造查询条件
     */
    public function buildparams()
    {
        $map = [];
        $params = input('get.');
        if (count($params) > 0) {
            foreach ($params as $k => $v) {
                if (in_array($k, ['page', 'limit', 'order'])) {
                    continue;
                }
                if ($v) {
                    $map[] = [$k, '=', $v];
                }
            }
        }
        return $map;
    }

    public function getWithArr()
    {
        return [];
    }

    public function getFields()
    {
        return '';
    }

    public function index($page = 1, $limit = 8, $sort = 'id desc')
    {
        $map = $this->buildparams();
        $withArr = $this->getWithArr();
        $query = $this->model->with($withArr)->where($map);
        $count = $query->count();
        $pages = 0;
        $list = [];

        if ($count == 0) {
            $result = ['count' => $count, 'pages' => $pages, 'list' => $list];
            return success($result);
        }
        $fields = $this->getFields();
        $list = $query
            ->field($fields)
            ->order($sort)
            ->page((int)$page)
            ->limit((int)$limit)
            ->select();

        $pages = ceil($count / $limit);
        $result = ['count' => $count, 'pages' => $pages, 'list' => $list];
        return success($result);
    }

    public function detail($id = 0)
    {
        $detail = $this->model::find($id);
        if ($detail == false) {
            return error('资源不存在');
        }
        return success($detail);
    }

    public function create()
    {
        $data = input('post.');
        try {
            validate($this->validateCalss)->check($data);
        } catch (ValidateException $e) {
            return error($e->getError());
        }
        $created = $this->model->create($data);
        if ($created) {
            return success($created);
        }
        return error($created);
    }

    public function update()
    {
        $data = input('post.');
        $id = input('post.id', 0);
        $model = $this->model->find($id);
        if ($model == false) {
            return error('资源不存在');
        }

        try {
            validate($this->validateCalss)->check($data);
        } catch (ValidateException $e) {
            return error($e->getError());
        }

        $saved = $model->save($data);
        if ($saved) {
            return success($saved);
        }
        return error($saved);
    }

    public function delete()
    {
        $ids = input('post.ids', '');
        if ($ids == false) {
            return error('ids不能为空');
        }
        $pk = $this->model->getPk();
        $list = $this->model->where($pk, 'in', $ids)->select();
        $count = 0;
        Db::startTrans();
        try {
            foreach ($list as $k => $v) {
                $count += $v->delete();
            }
            Db::commit();
        } catch (\PDOException $e) {
            Db::rollback();
        } catch (\Exception $e) {
            Db::rollback();
        }
        return success(['success' => $count]);
    }

    public function forceDelete()
    {
        $ids = input('post.ids', '');
        if ($ids == false) {
            return error('ids不能为空');
        }
        $pk = $this->model->getPk();
        $list = $this->model->where($pk, 'in', $ids)->select();
        $count = 0;
        Db::startTrans();
        try {
            foreach ($list as $k => $v) {
                $count += $v->force()->delete();
            }
            Db::commit();
        } catch (\PDOException $e) {
            Db::rollback();
        } catch (\Exception $e) {
            Db::rollback();
        }
        return success(['success' => $count]);
    }

    /**
     * 恢复删除的数据
     */
    public function restore()
    {
        $ids = input('post.ids', '');
        if ($ids == false) {
            return error('ids不能为空');
        }
        // $pk = $this->model->getPk();
        $list = explode(',', $ids);
        $count = 0;
        foreach ($list as $id) {
            $m = $this->model->onlyTrashed()->find($id);
            $count += $m->restore();
        }

        return success(['success' => $count]);
    }

    /**
     * 删除回收站数据
     */
    public function deletebin()
    {
        $ids = input('post.ids', '');
        if ($ids == false) {
            return error('ids不能为空');
        }
        // $pk = $this->model->getPk();
        $list = explode(',', $ids);
        $count = 0;
        foreach ($list as $id) {
            $m = $this->model->onlyTrashed()->find($id);
            $count += $m->force()->delete();
        }

        return success(['success' => $count]);
    }
}
